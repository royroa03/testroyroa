@extends('layouts.app')

@section('content')
<div class="container">
    <h2>
        Lista de Reservas
    </h2>
    <h6>
        @if($search)
        <div class="alert alert-primary" role="alert">
            Los resultados para tu búsqueda '{{$search}}' son:
        </div>
        @endif
    </h6>
    <table class="table table-hover">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Email</th>
            <th scope="col">Distrito</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Registro</th>
            <th scope="col">Cant.Personas</th>
            <th scope="col">Combo</th>
            <th scope="col">Comentario</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reservas as $reserva)
            <tr>
            <th scope="row">{{$reserva->id}}</th>
            <td>{{$reserva->nombre}}</td>
            <td>{{$reserva->apellido}}</td>
            <td>{{$reserva->email}}</td>
            <td>{{$reserva->distrito}}</td>
            <td>{{$reserva->telefono}}</td>
            <td>{{$reserva->fecha_registro}}</td>
            <td>{{$reserva->cantidad_persona}}</td>
            <td>{{$reserva->combo}}</td>
            <td>{{$reserva->comentario}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="mx-auto"> 
        </div>
    </div>
</div>
@endsection