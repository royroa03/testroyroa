<?php

namespace App\Http\Controllers;

use App\Reservas;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*
        if($request)
        {
            $query = trim($request->get('search'));
            $reservas = Reservas::select('reservas.*', 'combos.nombre as combo')
                    ->join('combos', 'combos.id', '=', 'reservas.id_combo')
                    ->where('reservas.nombre','LIKE','%'.$query.'%')
                    ->orderBy('reservas.id','asc')
                    //->simplePaginate(5);
                    ->paginate(10);
                    //->get();

            return view('reservas.index', ['reservas'=>$reservas,'search' => $query]);
        }
        //$reservas = Reservas::all();
        //return view('reservas.index', ['reservas'=>$reservas]);
        */
        $cliente= new Client(
            [
                'base_uri' => 'http://localhost/testroyroa_pedido/public/',
                'timeout' => 2.0,
            ]
        );

        $search = $request->get('search');
        if($search !='' || $search != null){
            $api = 'api/reportereservas/'.$search;
            $response = $cliente->request('GET',$api);
        }else{
            $response = $cliente->request('GET','api/reportereservas/');
        }
        $reservas = json_decode($response->getBody()->getContents());
        return view('reservas.index', ['reservas'=>$reservas->data,'search' => $search]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function show(Reservas $reservas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function edit(Reservas $reservas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reservas $reservas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Reservas  $reservas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservas $reservas)
    {
        //
    }
}
