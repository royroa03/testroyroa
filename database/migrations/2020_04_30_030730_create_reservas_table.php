<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('email');
            $table->string('distrito');
            $table->string('telefono');
            $table->dateTime('fecha_registro', 0);
            $table->string('cantidad_persona');
            $table->unsignedBigInteger('id_combo');
            $table->text('comentario');
            $table->timestamps();

            $table->foreign('id_combo')
                    ->references('id')
                    ->on('combos')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
