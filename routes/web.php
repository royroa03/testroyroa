<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::resource('reservas', 'ReservasController')->middleware('auth');
/*
Route::get('reportereservas', function(){
    $cliente= new Client(
        [
            'base_uri' => 'https://jsonplaceholder.typicode.com/',
            'timeout' => 2.0,
        ]
    );

    $response = $cliente->request('GET','posts');
    $posts = json_decode($response->getBody()->getContents());

});*/